INSERT INTO staff_category (description) VALUES
	('');
INSERT INTO staff (category_id, sex, job_title, first_name, middle_name, last_name, birth_date) VALUES
	(1,FALSE, 'Dean of Medicine','Lisa','','Cuddy','1970-06-11'),
	(2,TRUE, 'Sergient','Gregory','','House','1965-09-03'),
	(2,TRUE, 'Head of Department of Diagnostic Medicine','Gregory','','House','1965-09-03');
INSERT INTO addresses (building_number, street, city, country) VALUES
	(221,'Backer str','Houston','USA'),
	(-221,'Backer str','Odessa','USA'),
	(13,'Elm Street','Boston','USA'),
	(13,'Flit Street','Boston','USA');
INSERT INTO patient_payment_methods (description) VALUES
	('cash'),
	('credit card'),
	('');
INSERT INTO patients (payment_method_id, sex, birth_date, first_name, middle_name, last_name, height, weight, next_of_kin, mobile_phone) VALUES
	(1,FALSE,'1993-05-02','Oksana','Gennadievna','Tarasenko',173,56,'Olga, sister','+380111111111'),
	(1,FALSE,'1993-05-02','Oksana','Gennadievna','Tarasenko',-173,56,'Olga, sister','+380111111111'),
	(1,FALSE,'1993-05-02','Oksana','Gennadievna','Tarasenko',173,-56,'Olga, sister','+380111111111'),
	(1,FALSE,'1993-05-02','Oksana','Gennadievna','Tarasenko',173,56,'Olga, sister','11'),
	(2,TRUE,'1993-05-08','Andrii','Anatolievich','Hrushko',180,80,'Nelja, mother','+380222222222');
INSERT INTO patient_rooms (patient_id, room_id, date_stay_from, date_stay_to) VALUES
	(1,1,'2013-02-07','2013-04-28'),
	(2,2,'2012-01-01','2012-01-02');
INSERT INTO record_components (type, result) VALUES
	('','positive'),
	('drug screen','');
INSERT INTO patient_records (patient_id, component_id, updated_by_staff_id, medical_condition) VALUES
	(1,2,2,'she will be ok'),
	(2,1,1,'');