BEGIN;

DROP TABLE IF EXISTS staff_category CASCADE;
DROP SEQUENCE IF EXISTS staff_category_id_seq;
DROP TABLE IF EXISTS staff CASCADE;
DROP SEQUENCE IF EXISTS staff_id_seq;
DROP TABLE IF EXISTS addresses CASCADE;
DROP SEQUENCE IF EXISTS addresses_id_seq;
DROP TABLE IF EXISTS patient_payment_methods CASCADE;
DROP SEQUENCE IF EXISTS patient_payment_methods_id_seq;
DROP TABLE IF EXISTS patients CASCADE;
DROP SEQUENCE IF EXISTS patients_id_seq;
DROP TABLE IF EXISTS staff_addresses CASCADE;
DROP TABLE IF EXISTS patients_addresses CASCADE;
DROP TABLE IF EXISTS rooms CASCADE;
DROP SEQUENCE IF EXISTS rooms_id_seq;
DROP TABLE IF EXISTS patient_rooms CASCADE;
DROP SEQUENCE IF EXISTS patient_rooms_room_id_seq;
DROP TABLE IF EXISTS record_components CASCADE;
DROP SEQUENCE IF EXISTS record_components_id_seq;
DROP TABLE IF EXISTS patient_records CASCADE;
DROP SEQUENCE IF EXISTS patient_records_id_seq;

CREATE TABLE staff_category (
	id serial PRIMARY KEY,
	description text NOT NULL
	);

CREATE TABLE staff (
	id serial CONSTRAINT staff_id_pk PRIMARY KEY,
	category_id integer REFERENCES staff_category (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	sex boolean NOT NULL,
	job_title text NOT NULL CHECK (job_title!=''),
	first_name varchar(20) NOT NULL CHECK (first_name!=''),
	middle_name varchar(20),
	last_name varchar(20) NOT NULL CHECK (last_name!=''),
	birth_date date NOT NULL,
	other_details text,
	CONSTRAINT staff_name_uq UNIQUE (first_name, middle_name, last_name, birth_date)
	);

CREATE TABLE addresses (
	id serial PRIMARY KEY,
	building_number smallint NOT NULL CHECK (building_number>0),
	street varchar(40) NOT NULL CHECK (street!=''),
	city varchar(20) NOT NULL CHECK (city!=''),
	country varchar(20) NOT NULL CHECK (country!=''),
	other_details text,
	CONSTRAINT addresses_uq UNIQUE (city,country)
	);
	
CREATE TABLE patient_payment_methods (
	id serial PRIMARY KEY,
	description text NOT NULL
	);
	
CREATE TABLE patients (
	id serial PRIMARY KEY,
	payment_method_id integer REFERENCES patient_payment_methods (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	sex boolean NOT NULL,
	birth_date date NOT NULL,
	first_name varchar(20) NOT NULL CHECK (first_name!=''),
	middle_name varchar(20),
	last_name varchar(20) NOT NULL CHECK (last_name!=''),
	height smallint NOT NULL CHECK(height>0),
	weight smallint NOT NULL CHECK(weight>0),
	next_of_kin text NOT NULL,
	mobile_phone varchar(15) NOT NULL CHECK (bit_length(mobile_phone)>3),
	other_details text
	);

CREATE TABLE staff_addresses (
	staff_id integer REFERENCES staff (id) ON DELETE CASCADE ON UPDATE CASCADE,
	address_id integer REFERENCES addresses (id) ON DELETE RESTRICT ON UPDATE CASCADE
	);

CREATE TABLE patients_addresses (
	patient_id integer REFERENCES patients (id) ON DELETE CASCADE ON UPDATE CASCADE,
	address_id integer REFERENCES addresses (id) ON DELETE RESTRICT ON UPDATE CASCADE
	);
	
CREATE TABLE rooms (
	id serial PRIMARY KEY,
	description text
	);
	
CREATE TABLE patient_rooms (
	patient_id integer REFERENCES patients (id) ON DELETE CASCADE ON UPDATE CASCADE,
	room_id integer REFERENCES rooms (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	date_stay_from date NOT NULL,
	date_stay_to date NOT NULL
	);	
	
CREATE TABLE record_components (
	id serial PRIMARY KEY,
	type text NOT NULL,
	result text NOT NULL,
	done time DEFAULT now()
	);

CREATE TABLE patient_records (
	id serial PRIMARY KEY,
	patient_id integer REFERENCES patients (id) ON DELETE CASCADE ON UPDATE CASCADE,
	component_id integer REFERENCES record_components (id) ON DELETE SET NULL ON UPDATE CASCADE,
	updated_by_staff_id integer REFERENCES staff (id) ON DELETE NO ACTION ON UPDATE CASCADE,
	updated_date date DEFAULT now(),
	medical_condition text NOT NULL,
	other_details text
	);
COMMIT;	